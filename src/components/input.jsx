import React, { Component } from 'react'
import '../assets/input.css'
export class InputField extends Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <div className="card">
                <div className="container">
                    <input value={this.props.inputValue}onChange={this.props.changeInput}/>
                    
                    <button className="button" onClick={this.props.AddClicked}>Add</button>
                    
                </div>

            </div>
        )
    }
}

export default InputField;
