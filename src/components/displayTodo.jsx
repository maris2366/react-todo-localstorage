import React, { Component } from "react";
import '../assets/displayTodo.css'
export class DisplayTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
      
    return (
      <div className="overAllWrapper">
        {this.props.todoList &&
          this.props.todoList.map((data) => {
            return (
              <div className="wrapperContent" key={data + `${1}`}>
                <input className="inputTag" type="checkbox" checked={data.isChecked} value={data.value}  onChange={this.props.checkboxChange}/>
                <li className={data.isChecked === true ? "liStrike" :"liTag"} key={data}>{data.value}</li>
                <button className="delete" onClick={() => {this.props.delete(data.id)}}>Delete</button>
              </div>
            );
          })}
      </div>
    );
  }
}

export default DisplayTodo;
