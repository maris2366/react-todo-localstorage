import React, { Component } from "react";
import InputField from "./components/input.jsx";
import DisplayTodo from "./components/displayTodo.jsx";
import "./assets/app.css";
export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      inputValue: "",
    };
  }
  componentDidMount() {
    this.getLocalStorageItems();
  }
  getLocalStorageItems = () => {
    let todos = this.state.todos;

    if (localStorage.getItem("todos")) {
      let value = JSON.parse(localStorage.getItem("todos"));
      todos = [...value];
    }
    this.setState({
      todos,
    });
  };
  inputChange = (e) => {
    this.setState({
      inputValue: e.target.value,
    });
  };
  onAddClick = () => {
    let todos = this.state.todos;
    if (this.state.inputValue === "") {
      alert("Please Enter Your Todo and click submit");
    } else {
      todos.push({
        id: this.state.inputValue,
        value: this.state.inputValue,
        isChecked: false,
      });

      localStorage.setItem("todos", JSON.stringify(this.state.todos));
    }

    this.setState({
      todos,
      inputValue: "",
    });
  };

  handleCheckChildElement = (event) => {
    let todos = this.state.todos;

    todos.forEach((data) => {
      if (data.value === event.target.value) {
        data.isChecked = event.target.checked;
      }
    });
    localStorage.setItem("todos", JSON.stringify(this.state.todos));
    this.setState({ todos });
  };

  deleteTodo = (e) => {
    let todos = this.state.todos;
    let filtered = todos.filter(function (value, index, arr) {
      return value.id !== e;
    });

    localStorage.setItem("todos", JSON.stringify(filtered));

    this.setState({
      todos: [...filtered],
    });
  };
  render() {
    return (
      <div>
        <div className="app">
          <InputField
            changeInput={this.inputChange}
            AddClicked={this.onAddClick}
            inputValue={this.state.inputValue}
          />
        </div>
        {this.state.todos.length !== 0 && (
          <DisplayTodo
            todoList={this.state.todos}
            checkboxChange={this.handleCheckChildElement}
            delete={this.deleteTodo}
          />
        )}
      </div>
    );
  }
}

export default App;
